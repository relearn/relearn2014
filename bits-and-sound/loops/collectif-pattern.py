# Generates a gradient image in the ppm format
# Usage:
#     python collectif-pattern.py > myimage.ppm



print('P3') #   P3 = colored picture ; P2 = grayscale picture
print('51 5') #   set picture size
print('255') #   set a number of color


for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print(str(value)+ " " + '0' + " " + '255')
    print('23 00 255')
for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print('0' + " " + '0' + " " + '40')
    print('12 200 55')
for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print(str(value)+ " " + '0' + " " + '255')
    print('23 00 255')
for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print('0' + " " + '0' + " " + '220')
    print('12 200 55')
for value in range(255):      
    print (str(value)+' 0 ' + str(value))       
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print (str(value) + ' ' + str(value) + ' 0 ')             
for value in range(255):      
    print (str(value)+' 0 ' + str(value))       
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print (str(255 - value) + ' ' + str(value) + ' 0')      
    print (str(255 - value) + ' ' + str(value) + ' 0')      
    print ('0 0 0')                   
for value in range(255):      
    print ('0 ' + str(value) + ' ' + str(value))      
    print (str(255 - value) + ' ' + str(value) + ' 0')      
    print (str(255 - value) + ' ' + str(value) + ' 0')      
    print ('0 0 0')       
for value in range(255):    
    print ('0 ' + str(value) + ' ' + str(value))      
    print (str(255 - value) + ' ' + str(value) + ' 0')      
    print (str(25 - value) + ' ' + str(value) + ' 0')      
    print ('0 100 0')      
    print (str(25 - value) + ' 0 ' + str(value))      
    print ('0 100 200')      
    print ('0 100 200')     
    print (str(value)+' 0 ' + str(value)) 
    
# Alex
import math
for y in range(50):
    for x in range(50):
        val = int(((1 + math.cos(y * x)) / 2) * 255)
        print(val)
        print(val)
        print(val)        
        
for value in range(255):
    print("0 2 " + str(value))
    print("12 0" + " " + str(value))
    print('0 0 210')
    print('12 222 55')
for value in range (255):
    print("0 4 " + str(value))
    print("14 0" + " " + str(value))
    print('0 0 240')
    print('12 222 55')
