2084 Rise of the botnet
=======================

![Cindi Mayweather (aka Janelle Monae)](http://d243395j6jqdl3.cloudfront.net/wp-content/uploads/2013/10/Janelle_Monae2.png "Cindi Mayweather (aka Janelle Monae)")
![ELIZA](http://upload.wikimedia.org/wikipedia/commons/9/98/GNU_Emacs_ELIZA_example.png "ELIZA")
![](http://upload.wikimedia.org/wikipedia/en/thumb/e/e5/The-naked-sun-doubleday-cover.jpg/220px-The-naked-sun-doubleday-cover.jpg "R. Daneel Olivaw")
![Futura (Metropolis)](http://upload.wikimedia.org/wikipedia/en/thumb/d/da/Metropolisnew.jpg/170px-Metropolisnew.jpg "Futura (Metropolis)")


* [Ten ways to network (using ssh)](Ten_things_to_do_with_SSH.html)
* [Ten things to do with IRC (2084 introduction)](Ten_things_IRC.html)
* [Speculative Bot Portraits](portraits_piratebots_def.html)
* [How to publish a website with git](How_to_publish_a_website_with_git.html)
* [How to create an openWRT network node](How_to_create_an_openWRT_network_node.html)

