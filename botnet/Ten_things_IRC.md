
From the team of '2084: The Rise of the Botnet'
==================================================

Introduction to the Botnet
------------------------------

There is a life after Google & the Cloud! 

Botnet brings you back to your real peers, without excluding you from communicating with the entire world. Botnet allows for the intimacy of a physical environment online. Each user of Botnet can regulate what comes in and goes out, where it goes, at what speed, under which circumstances.
The default format of the Botnet is Written Speech. By using IRC channels to document workflows or exchange information, you stay in the flow of the physical interactions. The Botnet relies on the IRC software that has been around for almost a century now. It is still highly flexible, efficient and modulable.

Horizontal and vertical channeling allows for highly personal costumized user settings and services. Robots allow for all kinds of interactions, anonymous or not, which other virtual spaces. 
You choose, you create, you use!


Ten marvellous things you can do with IRC
------------------------------


### 1. Connect to the world

Internet Relay Chat (IRC) is a system that facilitates transfer of messages in the form of text. It was invented by Jarkko Oikarinen in 1988 and is still used worldwide by thousands of people. 
The chat process works on a client/server model of networking. IRC clients are computer programs that a user can install on their system. These clients are able to communicate with chat servers to transfer messages to other clients. IRC consists of various separate networks (or “nets”) of IRC servers, machines that allow users to connect to IRC. The largest nets are EFnet (the original IRC net, often having more than 32,000 people at once on more than 12,000 channels, each devoted to a different topic), Undernet, IRCnet, DALnet, and NewNet. Freenode offers discussion facilities for the Free and Open Source Software communities.
IRC is mainly designed for group communication in discussion forums, called channels, but also allows one-to-one communication via private message as well as chat and data transfer, including file sharing. From: http://en.wikipedia.org/wiki/Internet_relay_chat
We advise you to install the IRC client Konversation.

### 2. Connect to Variable's Bot Net featuring: Cindi, Daneel, Eliza-Eugene, Futura

For the Relearn Summerschool the team of 2084: The Rise of the Botnet set up a series of workspaces you can connect to via IRC. The idea is to choose your workspace consciously, it can be one of these local workspaces near your table, the local server of Variable or any online space. 
As for the workspaces, each one is managed by a different Famous Bot from History. Their personalities might influence your way of writing on the workspace. You find their portraits as ICR-logs from the future on the fileserver of each workspace. 
As part of the project 2084: The Rise of the Botnet  this Bot Net can be extended with more devices during the Summerschool. You are also invited to learn how to create IRC-Bots that can publish your notes online for you.
To connect to Cindi, Daneel, Eliza-Eugene, Futura, just select them on your network configuration, and you're online (no password).



### 3. Create your own IRC channel 

You can create an IRC channel for your project during Summerschool. We advise you to do this on one of the workspaces mentioned above. The conversations on this channel might be logged on the local Variable server. 
Be aware: once you log out of your channel, the first user who logs back in, will have all super powers as a channel operator (see 4).


### 4. Be the channel operator

A channel operator is someone with a “@” by their nickname in a /names list, or a “@” by the channel name in /whois output. Channel operators are kings/queens of their channel. This means they can kick you out of their channel for no reason. If you don’t like this, you can start your own channel and become a channel operator there.
As a channel operator you can organise the power relationships of the users in your channel. You can f.ex. :
grant them full permissions: /MODE #name_channel +o name_user
connect to another computer without passing through a public channel or server: /DCC CHAT name_user
make your channel go underground (secret)*: /MODE #name_channel +s
turn your channel public again (default):  /MODE #demo -s

*IRC gained international fame during the 1991 Persian Gulf War, where updates from around the world came accross the wire, and most irc users who were online at the time gathered on a single channel to hear these reports. IRC had similar uses during the coup against Boris Yeltsin in September 1993, where IRC users from Moscow were giving live reports about the unstable situation there. 
https://publicintelligence.net/tactical-chat/

Full documentation on channel operators: http://irchelp.org/irchelp/changuide.html


### 5. Maintain hybrid identities

On IRC you can change your nickname as many times you want by typing /NICK new-nickname.
By using the command /WHOIS my_nickname you can see all information you give away to others; and you can also see information about other users by typing /WHOIS username.
When doing a /whois on yourself you might notice that your real name shows up. If you don't like this, you can change it following these instructions: 
In UNIX, there are two way of changing your IRCNAME and it depends on which shell you are using. If you are using csh or tcsh (the more popular UNIX shells, when in doubt, try this first), type this before you start irc:
setenv IRCNAME “what you would like to appear”
If you don’t want to type that every time you log in, put the line exactly as it appears above into your .cshrc file.
If you are using sh, ksh, or bash, type this before you start irc: IRCNAME=”what you would like to appear”;export IRCNAME
Or insert that line into your .profile
In VMS, you must put this line in your login.com file: DEFINE IRCNAME “what you would like to appear”


### 6. IRC-bots



An IRC bot is a set of scripts or an independent program that connects to Internet Relay Chat as a client, and so appears to other IRC users as another user. An IRC bot differs from a regular client in that instead of providing interactive access to IRC for a human user, it performs automated functions. 
A bot generally tries to “protect” a channel (it should be noted that all bots will fail at some point, so relying on them to keep a channel is not a good idea) from takeovers. 
IRC is polyglot! You can launch scripts in a large variety of languages :-)

The historically oldest IRC bots were Bill Wisner's Bartender and Greg Lindahl's GM (Game Manager for the Hunt the Wumpus game). Over time, bots evolved to provide special services, such as managing channels on behalf of groups of users, maintaining access lists, and providing access to databases.


### 7. 2084: customized IRC-bots

Instead of using IRC-bots in the classical way, to protect against evil forces, we invite you to use IRC-bots as agents who can publish for you on online platforms (etherpad, gitorious...), report back to your channel when someone has published on one of the platforms, look for useful links in browsers (f.ex. Googlebot), rephrase information, etc. 
You find a first series of bots here: https://gitorious.org/activearchives/botbotbot
Instructions to launch a Python bot:
$ sudo pip install irc 
$ cd /path_to_folder_where_your_bot_is_saved
$ python name_bot.py url_server name_channel name_bot_in_channel, f.ex. python tob2.py irc.freenode.net '#VJ14' tob 
To stop running the bot:
ctrl+c

Some examples of programming an IRCbot in Python: http://pzwart3.wdka.hro.nl/wiki/IRC


### 8. Study the language of IRC and find ways to reinvest it

As friendly, hybrid and flexible the IRC-architecture is, as rigid and warriorlike the language sounds that is used in its programming. As IRC is fully OS, you can rewrite the code in a more feminist friendly way.
A channel operator f.ex. can not only kick you, she can also ban you and kill you!!!
“Ghosts are not allowed on IRC” means that you are banned from using that server. “The banning is in one of three forms:
You are banned specifically, you yourself. Only you can be responsible for this (if you are using a shared account, this obviously does not apply). Thus the responsibility lies completely with you and you have noone to complain to. 
Your machine is banned. Chances are it wasn’t you who committed the wrongdoing. Try using another machine on campus and seeing if you can use that particular irc server then. 
Your whole site is banned (where “site” == “school”, “company”, “country”). This almost certainly wasn’t your fault. And chances are you won’t be able to get the server-ban lifted. Try using another server. The most general answer is “use another server”, but if it bothers you, try writing to the irc administrator of that site —> /admin server.name.here — plead your case. It might even get somewhere! “


### 9. Hack the MOTD

Hack the Message Of The Day...
The local IRC in Variable welcomes you with a MOTD that was written by Anne. 
Originally the MOTD is meant to inform users about the constant changes on IRC: “The way things to work one week may not be the way they work the next. Read the MOTD (message of the day) every time you use IRC to keep up on any new happenings or server updates.”
To edit the MOTD file on the local server : 
$ nano /etc/ircd-hybrid/ircd.motd 


### 10.  Install IRC & Start reading TFM NOW
 
You can install IRC on your computer
client: we recommend: Konversation
First, check to see if irc is installed on your system. Type “irc” from your prompt. If this doesn’t work, ask your local systems people if irc is already installed. This will save you the work of installing it yourself. If an IRC client isn’t already on your system, you either compile the source yourself, have someone else on your machine compile the source for you.

Learn EVERYTHING on IRC:
as a book:
http://irchelp.org/irchelp/ircprimer.html
as a tutorial:
http://irchelp.org/irchelp/irctutorial.html
Advanced user guide:
http://irchelp.org/irchelp/misc/ccosmos.html


