import irc.bot
import irc.client
import time
from lineParser import lineParser

class CircleBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, plot_channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.plot_channel = plot_channel
        
    def on_welcome(self, c, e):
        print "Bot connected"
        c.join(self.channel)
        c.join(self.plot_channel)

    def on_privmsg(self, c, e):
        self.on_pubmsg(c, e)

    def on_pubmsg(self, c, e):
        msg = e.arguments[0].decode("utf-8")
        
        lines = lineParser(msg, steps=100)
        buff = ''
        for line in lines:
            if len(buff) + len(line.format) > 400:
                c.privmsg(self.plot_channel, buff)
                time.sleep(.5)
                buff = line.format
            else:
                buff += line.format
                
        c.privmsg(self.plot_channel, buff)
        c.privmsg(self.channel, "Thanks check {0} to see the result".format(self.channel))
        
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='BashBot')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('channel', help='channel to join')
    parser.add_argument('plotchannel', help='plotter channel')
    parser.add_argument('nickname', help='bot nickname')
    args = parser.parse_args()
    
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel
        
    bot = CircleBot(args.channel, args.plotchannel, args.nickname, args.host, args.port)
    bot.start()