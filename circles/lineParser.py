"""
    Takes a string and returns a collection of chiplotle paths

    [
    [[0,0.25],[.75,1]],
    [[.3,.5],[.8, 1]]
    ]

    ___0.1 ->.5 0.75->1___ ___ ___ ___0.1->.2 0.3->0.4 0.5->.6 .8->.9___

    '---'

    ---(\d+.?\d*)----

    ---()----
    explode on spaces
    explode on ->

    ---\s

    (\-\-\-(?:(?:.[^\-\-\-]*)\-\-\-)?)

    (?:\d*\.*\d+\->\d*\.*\d+\s?)+

    (___(?:(?:\d*\.*\d+\->\d*\.*\d+\s?)+___)?)


    ---0.1--0.5 0.75--1--- --- --- ---0.1--.2 0.3--0.4 0.5--.6 .8--.9---
"""

import re
import math
from chiplotle.geometry.transforms.rotate import rotate
from chiplotle.geometry.transforms.offset import offset
from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.coordinate import Coordinate

#debug = True

#if debug:
    #plotter = plottertools.instantiate_virtual_plotter(type="HP7550A")
    #margins = plotter.margins.soft
    #width = margins.right - margins.left
    #height = margins.top - margins.bottom

##msg = '--- ---0--.25 .9--1--- --- ---.1--.2 .3--.4 .5--.        6 .7--.8 .9--1---'

#lines = []

#steps = 50
#length = 3000

def lineParser(lineString, steps=50, length=4000, width=16000, height = 8000):
    result = []
    lines = []
    
    linepatt = '\[(.[^\]]*)\]'
    rangepatt = '{(\d+):(\d+)}'
    coordpairpatt = '\S+->\S+'
    coordpatt = '(.*)->(.*)'

    for line in re.findall(linepatt, lineString):
        segments = []
        drawrange = (0, 360)
        
        if line == 0:
            continue
        elif line == 1:
            segments.append(('0', '1'))
        else:
            segments = []
            foundrange = re.findall(rangepatt, line)
            
           
            
            if len(foundrange) > 0:
                drawrange = (int(foundrange[0][0]), int(foundrange[0][1]))
                
            coordpairs = re.findall(coordpairpatt, line)
            
            for coordpair in coordpairs:
                coords = re.findall(coordpatt, coordpair)
                start = coords[0][0]
                end = coords[0][1]
                
                segments.append((start, end))
       
        lines.append({
            'segments': segments,
            'drawrange': drawrange
        })
    
    l = -1
    
    for i in range(steps):
        if len(lines) > 0:
            a = (float(i) / float(steps)) * math.pi * 2
            ad = (float(i) / float(steps)) * 360
            l = l+1 if l+1<len(lines) else 0
            line = lines[l]
            
            if line['drawrange'][0] <= ad and ad <= line['drawrange'][1]:
                for segment in line['segments']:
                    path = Path([Coordinate(eval(segment[0]) * length, 0), Coordinate(eval(segment[1]) * length, 0)])
                    rotate(path, a)
                    offset(path, Coordinate(width * .5, height * .5))
                    result.append(path)
        
    return result

if __name__ == "__main__":
    from chiplotle.geometry.core.path import Path
    from chiplotle.geometry.core.coordinate import Coordinate
    print lineParser('[{0:180} 0->1]')