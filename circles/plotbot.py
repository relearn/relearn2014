#!/usr/bin/env python2
import irc.bot
import chiplotle

class PlotBot(irc.bot.SingleServerIRCBot):
    def __init__(self, virtual, plotter, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.plotter = plotter
        self.channel = channel
        self.virtual = virtual

    def on_welcome(self, c, e):
        print "Bot connected"
        c.join(self.channel,)

    def on_privmsg(self, c, e):
        self.on_pubmsg(c, e)

    def on_pubmsg(self, c, e):

        msg = e.arguments[0].decode("utf-8")

        if msg.find("?")>=0:
            c.privmsg(self.channel, "Resource: http://mercator.elte.hu/~saman/hu/okt/HPGL.pdf")           
        else:
            self.plotter.write(str(msg))
        
        if self.virtual:
            chiplotle.io.view(self.plotter)

    def on_join(self, c, e):
        c.privmsg(self.channel, "Welcome to the Plotbot IRC channel!")
        c.privmsg(self.channel, "To operate the plotter input your specific commands now.")
            
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='BashBot')
    parser.add_argument('--virtual', action='store_true', default = False,
        help='Run the plot bot in virtual mode instead of in hardware mode.')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('channel', help='channel to join')
    parser.add_argument('nickname', help='bot nickname')
    args = parser.parse_args()
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel
    if args.virtual:
        plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter(type="HP7550A")
    else:
        plotter = chiplotle.instantiate_plotters()[0]
        
    bot = PlotBot(args.virtual, plotter, args.channel, args.nickname, args.host, args.port)
    bot.start()

