#!/usr/bin/env python2
import itertools
from functools import partial
import math

import chiplotle

def init(virtual = False):
    '() -> ([angle] -> (angle -> radius) -> IO ())'
    if virtual:
        plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter(type = "HP7550A")
    else:
        plotter = chiplotle.instantiate_plotters()[0]
    return plotter, partial(plot_centered_plot, plotter, plotter.margins.soft.all_coordinates)

def plot_centered_plot(plotter, all_coordinates, angles, function):
    plotter.write(compose_centered_plot(all_coordinates, angles, function))

def compose_centered_plot(all_coordinates, angles, function):
    '''
    For a given plotter (plotter) with a particular
    range of plotting coordinates (all_coordinates),
    evaluate a function (angle -> radius) at a each
    of a series of angles.

    The function receives an angle in radians and
    returns a radius between 0 and 1.
    '''
    is_first = True
    command = 'PU'
    for angle in angles:
        coords = denormalize(all_coordinates, angle, function(angle))
        if coords == None:
            command += ';PU'
        else:
            if is_first:
                command += ';PD%d,%d' % coords
                is_first = False
            else:
                command += ',%d,%d' % coords
    command += ';'
    return command

def denormalize(all_coordinates, angle, radius):
    if radius == None:
        return None
    elif not (-1 <= radius <= 1):
        raise ValueError('Radius must be between -1 and 1.')
    xmin, ymin, xmax, ymax = all_coordinates
    x_center, y_center = ((xmax - xmin) / 2), ((ymax - ymin) / 2)
    max_radius = min(ymax - ymin, xmax - xmin) / 2
    
    r = radius * max_radius
    x = r * math.cos(angle)
    y = r * math.sin(angle)
    return (x_center + x), (y_center + y)

def angles(steps_per_rotation, rotations = 1):
    return [2*x*math.pi/steps_per_rotation for x in range(0,rotations * steps_per_rotation)]

def one():
    plotter, p = init()
    steps = 200

    plotter.write('PU;SP3;')
    rotations = 6
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.8 * math.sin(angle/rotations))

    plotter.write('PU;SP2;')
    rotations = 1
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.5 + 0.3 * math.cos(angle/rotations))

    plotter.write('PU;SP1;')
    rotations = 4
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.1 + 0.7 * math.cos(angle/rotations))

def two():
    plotter, p = init()
    steps = 200

    plotter.write('SP3;')
    rotations = 6
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.8 * math.sin(angle/rotations) if abs(math.cos(angle)) > 0.5 else None)

    plotter.write('PU;SP2;')
    rotations = 1
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.5 + 0.3 * math.cos(angle/rotations))

    plotter.write('PU;SP1;')
    rotations = 4
    xs = angles(steps, rotations)
    p(xs, lambda angle: 0.1 + 0.7 * math.cos(angle/rotations))


def spiral(xmin, ymin, xmax, ymax, max_rotations, rotations, degrees):
    '''
    max_rotations: the number of rotations in the largest circle possible
    rotations: the number of rotations in the circle you're drawing
    degrees: like in HPGL---90 degrees draws a square
    '''
    steps = int(360 / degrees)
    all_coordinates = xmin, ymin, xmax, ymax
    f = lambda a: a / max_rotations / (2 * math.pi)
    return compose_centered_plot(all_coordinates, angles(steps, rotations), f)
