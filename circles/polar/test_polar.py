import math
import nose.tools as n

import polar

def test_compose():
    rotations = 2
    observed = polar.compose_centered_plot((1000,2000,1400,3300), polar.angles(4, rotations), lambda a: a / rotations / (2 * math.pi))
    expected = 'PU;PD200,650,200,675,150,650,200,575,300,650,200,775,50,650,199,475;'
    n.assert_equal(observed, expected)

def test_spiral():
    observed = polar.spiral(1000,2000,1400,3300, 20)
    expected = ''
    n.assert_equal(observed, expected)
