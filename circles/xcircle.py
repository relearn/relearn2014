#!/usr/bin/env python

from random import randint
from math import sqrt

"""
To run:
python circle.py > circle.html

"""

print """<style>
span {
position: absolute;
}
</style>
<div style="position: relative;">
"""

for i in range(10000):
    x = randint(0, 1000)
    y = randint(0, 1000)
    dist = sqrt( (x-500)**2 + (y-500)**2 )
    # print dist
    if (dist < 250):
        print """<span style="left: {0:d}px; top: {1:d}px">x</span>""".format(x, y)


print """</div>

<div style="height: 200px"></div>

"""
