/*
 * Just a little scketch to test the relation between colors and integers in Processing
 */

// Give a color a integer number
println("Half transparent black (in hex) 0x80000000 -> " + 0x80000000);
println("Full transparent black          0x00000000 -> " + 0x00000000); 
println("Half transparent white          0x7FFFFFFF -> " + 0x7FFFFFFF);
println("");
println("Opaque black                    0xFF000000 -> " + 0xFF000000);
println("Opaque white                    0xFFFFFFFF -> " + 0xFFFFFFFF);

// Creating a random image
PGraphics img;
int size = 3;
img = createGraphics(size, size);
img.beginDraw();
img.background(0x80000000);

int[] data = new int[size*size];

for (int i=0; i < size*size; i++){
  int tempInt = int(random(-2147483648, 2147483647));
  img.set(i%3, i/3, tempInt );
  data[i] = tempInt;
}

img.endDraw();
println("Random data for the image");
println(data);
img.save("data/test.png");


PImage pix;
pix = loadImage("data/test.png");
pix.loadPixels();
println("Data from the image");
println(pix.pixels);
