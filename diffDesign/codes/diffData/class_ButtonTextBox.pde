////thsi is a class to create a text box to search twitter for recent images
class buttonText{
  
  int posX=0;
  int posY=screenGUI/4;
  int dimX=width;
  int dimY=guiDim;
  String textValue;

  buttonText(){
    textValue = searchTerm;
  }
  
  void update(){
    textValue = searchTerm;
  }
  
  void display(){
    fill(255,0,0);
    noStroke();
    rect(posX, posY, dimX, dimY);
    
    fill(255);
    textAlign(LEFT, TOP);
    textSize(dimY/2);
    text(textValue, posX, posY); 
  }
  
}

//////////////////////////////////////////////////////////////////
void initializeTextBox(){
  buttonTextBox = new buttonText();
}

