////a class for each file, loading the pixel data and the display of the graph and the model
class pixModel{ 
  
  int liveIndex=1;
  int index;
  
  PImage pixData;
  PGraphics pg;
  String[] pixRowArray = new String [screenH];
  int[]  pixRowStatus = new int [screenH];
  
  int[] [] matchInd = new int [3][screenH];
  boolean[] [] makeCurve = new boolean [3][screenH];
  
  float xRange;
  float yRange; 
  float zRange;
  
  float minX;
  float minY;
  float minZ;
  float maxX;
  float maxY;
  float maxZ;
   
  boolean dispMesh;
  boolean dispGraph;
  
  pixModel(int ind, boolean graph, boolean mesh){
    index = ind;
    dispGraph = graph;
    dispMesh = mesh;
  }
  
  void getPixData(){
    
    minX=1279999;
    minY=1279999;
    minZ=1279999;
    maxX=-1279999;
    maxY=-1279999;
    maxZ=-1279999;
    
    pixData = loadImage(pixList.get(liveIndex));
    
    clear();
    image(pixData, 0, 0);
    loadPixels();
    updatePixels();
    
//    println("viewer" + index+"...load new file.... " + pixList.get(liveIndex));
//    println(pixData.width + " - " + pixData.height);

    for (int y=pixData.height-1; y>=0 ;y--){
      //println("y-" + y);
      pixRowArray[y] = "#";
          
      for (int x=0; x<pixData.width; x+=3){
        //println("x-" + x);
        color testCol = get(x, y);
        int hX = int(red(testCol));
        int hY = int(green(testCol));
        int hZ = int(blue(testCol));
        
        //println("y-" + y + " x-" + x+ "  " + hY + "-" + hY + "-" + hZ);
        
        if ((hX<199) && (hY<199) && (hZ<199)){
 
          for (int p=0; p<3;p++){
            //println("p-" + p);
            //println("x-" + x + "y-" + y + "p-" + p);
            
            color tempCol = get(x+p, y);

            String hueRStr;
            String hueGStr;
            String hueBStr;
            
            boolean negVal=false;

            if (red(tempCol)>=100){
              hueRStr = nf(int(red(tempCol))-100,2);
            } else {
              hueRStr = nf(int(red(tempCol)),2);
            }
            if (green(tempCol)>=100){
              hueGStr = nf(int(green(tempCol))-100,2);
            } else {
              hueGStr = nf(int(green(tempCol)),2);
              negVal=true;
            }
            if (blue(tempCol)>=100){
              hueBStr = nf(int(blue(tempCol))-100,2);
            } else {
              hueBStr = nf(int(blue(tempCol)),2);
              negVal=true;
            }
            
            int tempVal;
            if (negVal==true){
             tempVal = -1 * int(hueRStr+hueGStr+hueBStr);
            } else {
              tempVal = int(hueRStr+hueGStr+hueBStr);
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ((red(tempCol)<199) && (green(tempCol)<199) && (blue(tempCol)<199)){
              pixRowArray[y] =  pixRowArray[y] + "#" + str(tempVal);
            } else {
              pixRowArray[y] =  pixRowArray[y] + "#" + 99;
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (p==0){
              if (tempVal>maxX){ maxX=tempVal;}
              if (tempVal<minX){ minX=tempVal;}
            }
            if (p==1){
              if (tempVal>maxY){ maxY=tempVal;}
              if (tempVal<minY){ minY=tempVal;}
            }
            if (p==2){
              if (tempVal>maxZ){ maxZ=tempVal;}
              if (tempVal<minZ){ minZ=tempVal;}
            } 
      
          }          
        }
      }
      //println( pixRowArray[y]);
    }
    
    xRange = maxX-minX;
    yRange = maxY-minY;
    zRange = maxZ-minZ;
//    println(xRange + "<>" + yRange + "<>" + zRange);

    println("end");

  }
    
  void displayDiffMesh(){
    
    if (dispMesh==true){
     
      float[] colPosA = new float[3];
      float[] colPosB = new float[3];
      float[] colPosC = new float[3];
      
      int faceCount=0;
      int matchIndex;
     
      directionalLight(125, 125, 125, 0, 1, -1);
      ambientLight(175,175,175);
      strokeWeight(1/SCALE);
      stroke(0,20); 
      
      for (int y=0; y<pixRowArray.length ;y++){
        println("y-" + y);
       
        if(index==1){
          if(pixRowStatus[y]==0){fill(255,0,255);}
          if(pixRowStatus[y]==1){fill(255,255,0);}
          if(pixRowStatus[y]==3){fill(0,255,255);}
          if(pixRowStatus[y]==4){fill(255,255,255);}
        } else {
          fill(255);
        }
        
        String[] tempPos = splitTokens(pixRowArray[y], "#"); 
        
        println(tempPos.length);
        
        if (tempPos.length==9){
          for (int x=0; x<tempPos.length ;x+=9){ 
            
            println("x-"+x);
  
            colPosA[0]= float(tempPos[x+0]); 
            println("x+0");
            colPosA[1]= float(tempPos[x+1]);
            println("x+1"); 
            colPosA[2]= float(tempPos[x+2]);
            println("x+2");
            
            colPosB[0]= float(tempPos[x+3]); 
            println("x+3");
            colPosB[1]= float(tempPos[x+4]);
            println("x+4"); 
            colPosB[2]= float(tempPos[x+5]); 
            println("x+5");
            
            colPosC[0]= float(tempPos[x+6]); 
            println("x+6");
            colPosC[1]= float(tempPos[x+7]); 
            println("x+7");
            colPosC[2]= float(tempPos[x+8]); 
            println("x+8");
           
            
            beginShape();
              vertex(colPosA[0], colPosA[1], colPosA[2]);
              vertex(colPosB[0], colPosB[1], colPosB[2]);
              vertex(colPosC[0], colPosC[1], colPosC[2]);
            endShape(CLOSE);
              
            faceCount++;
          
          }
        }
  
      }    
      noLights(); 
    } 
  }
  
  void drawCurves(){
    
    for (int y=0; y<pixRowArray.length ;y++){
      for (int n=0; n<3; n+=2){
        if (makeCurve[n][y]==true){

          String[] tempCol = splitTokens(pixRowArray[y], "#"); 
          String[] tCol = splitTokens(pixMod[n].pixRowArray[matchInd[n][y]], "#");
          
          for (int x=0; x<tempCol.length ;x+=9){
                 
            pushMatrix();  
              translate((width/6)+(width/3), height/2);
              rotateY(ROTX);
              rotateX(ROTY);
              scale(SCALE);
              float myPosX = ((int(tempCol[x+0]))+(int(tempCol[x+3]))+(int(tempCol[x+6])))/3;
              float myPosY = ((int(tempCol[x+1]))+(int(tempCol[x+4]))+(int(tempCol[x+7])))/3;
              float myPosZ = ((int(tempCol[x+2]))+(int(tempCol[x+5]))+(int(tempCol[x+8])))/3;
              float tempMyPosX = screenX(myPosX, myPosY, myPosZ);
              float tempMyPosY = screenY(myPosX, myPosY, myPosZ);  
            popMatrix();
            
            pushMatrix();  
              translate((width/6)+(n*(width/3)), height/2);
              rotateY(ROTX);
              rotateX(ROTY);
              scale(SCALE);            
              float posX = ((int(tCol[x+0]))+(int(tCol[x+3]))+(int(tCol[x+6])))/3;
              float posY = ((int(tCol[x+1]))+(int(tCol[x+4]))+(int(tCol[x+7])))/3;
              float posZ = ((int(tCol[x+2]))+(int(tCol[x+5]))+(int(tCol[x+8])))/3;       
              float tempPosX = screenX(posX, posY, posZ);
              float tempPosY = screenY(posX, posY, posZ); ;
              float tempPosMapY = map(posZ, minZ, maxZ, screenH, 0) + (screenGUI/2);
            popMatrix();
            
            strokeWeight(10);
            noFill(); 
            stroke(255);

            if(pixRowStatus[y]==0){stroke(255,0,255,5);}
            if(pixRowStatus[y]==1){stroke(255,255,0,5);}
            if(pixRowStatus[y]==3){stroke(0,255,255,5);}
            if(pixRowStatus[y]==4){stroke(255,255,255,5);}
        
            if (n==0){ 
               bezier(tempMyPosX, tempMyPosY, tempPosX, tempPosY, tempPosX, tempPosY, 0, tempPosMapY);
            } else {
               bezier(tempMyPosX, tempMyPosY, tempPosX, tempPosY, tempPosX, tempPosY, screenW, tempPosMapY);
            }         
          }  
        }
      }
    }
  }
  
  void displayBoundingBox(){
    noFill();
    stroke(255,20);
    strokeWeight(1/SCALE);
    box(xRange+10, yRange+10, zRange+10);
  }
  
  void makeGraph(){   
    
    pg = createGraphics(width, height);
    pg.beginDraw();
    pg.background(255,0);
    pg.strokeWeight(1);
    pg.stroke(255);
    
    for (int y=0; y<pixRowArray.length ;y++){;
    
      int pixY=y+(screenGUI/2);
      String[] tempCol = splitTokens(pixRowArray[y], "#");
       
      if (index==0){ pg.line(0, pixY, tempCol.length, pixY);}
      if (index==1){ pg.line((screenW/2)-(tempCol.length/2), pixY, (screenW/2)+(tempCol.length/2), pixY);}
      if (index==2){ pg.line(screenW, pixY, screenW-tempCol.length-1, pixY);}
      
    }
      
    pg.endDraw();
    pg.save("data/loaded/pixGraph-"+index+"-loaded.png");
    
  }
   
  void getDiffStatus(){

    for (int y=0; y<pixRowArray.length ;y++){
      pixRowStatus[y]=0;      
      for (int n=0; n<3; n+=2){
        for( int yy=0; yy<pixRowArray.length ;yy++){  
          makeCurve[n][y]=false;
          matchInd[n][y]=0;
          if ((pixRowArray[y].equals(pixMod[n].pixRowArray[yy])==true)&&(pixRowArray[y].equals("#")==false)) {
            matchInd[n][y]=yy;
            makeCurve[n][y]=true;
            pixRowStatus[y]+=(n+1);
            break;
          }
        }
      }
    }
    
  }

  
}

//////////////////////////////////////////////////////////////////
void intializePixMod(){
  println("initializePixMod");
  for (int i=0; i<3; i++){
    if (i==1){
      pixMod[i] = new pixModel(i, false, true);
    } else {
      pixMod[i] = new pixModel(i, true, false);
    }
  }
}



