int screenH = 600;
int screenGUI = 80;
int guiDim= 20;
int screenW = 1200;

pixModel[] pixMod = new pixModel[3];
buttonSlider[] buttonSlide = new buttonSlider[3];
buttonClicker[] buttonClick = new buttonClicker[3];
buttonText buttonTextBox;

boolean getPixData=false;
boolean loadNewPix=true;

String path;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup(){

  noLoop();
  size(screenW, screenH+screenGUI, OPENGL);
  
  path = savePath("");
  println(path);
  
  getFileList();
  
  initializeSliders();
  initializeButtons();
  initializeTextBox();
  intializePixMod();
  
  //////////////////establish twitter connection
  //connectTwitter();
  //////////////////establish twitter connection
 
}

void draw(){
  ///search twitter for new images, according to the button controls
//  if (searchTwitter==true){
//    twitterSearches();
//    searchTwitter=false;
//  }
  
  checkUpdates();
  println("checkedUpdates");
  
  background(0);
  ////draw the search box, if typing
  if (showSearch==true){   
    buttonTextBox.display();
    showSearch=false;
  }
  ////draw the buttons
  for (int i=0; i<3; i++){
    buttonSlide[i].display();
    buttonClick[i].display();
  }
  
  ////dispaly the model, according to the button controls
  
  for (int i=0; i<3; i++){
    pushMatrix();
      translate((width/6)+(i*(width/3)), height/2);
      rotateY(ROTX);
      rotateX(ROTY);
      if ((i==0)||(i==2)){pixMod[i].displayDiffMesh();} 
      scale(SCALE);
      if (i==1) {pixMod[1].displayDiffMesh();}
    popMatrix();
  }  
  
  ////dispaly the graph, according to the button controls
  for (int i=0; i<3; i++){
    if (pixMod[i].dispGraph==true){
      image(pixMod[i].pg, 0, 0);
    }
  }
  ////middle model draws the differnece lines to the previous and future versions
  pixMod[1].drawCurves();
  
  ////send a tweet, according to the button controls
  if(makeTweetImage==true){
    makeTweetImage();
    tweetNewImage();
    makeTweetImage=false;
  }
  
}





