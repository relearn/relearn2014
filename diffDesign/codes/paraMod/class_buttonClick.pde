////a class for a cliack-able button - this one controls the drop down list of parameters
class buttonClicker{
  
  int posX=0;
  int posY=0;
  int dimX=screenGUI;
  int dimY=sliderHeight;
 
  boolean active=true;
  
  void update(){
     if (active==false){
        active=true;
      } else {
        active=false;
      }  
  }
  
  void display(){
    
    strokeWeight(1);
    stroke(0);
    fill(255,0,0);    
    rect(posX,posY,dimX, dimY);
   
  }
  
}

//////////////////////////////////////////////////////////////////
void initializeButtons(){
  buttonClick[0]= new buttonClicker();
}


