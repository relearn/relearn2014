int screenW = 1000;
int screenH = 600;
int screenGUI = 200;

int sliderHeight=15;

int activeSliders=20;

buttonSlider[] buttonSlide = new buttonSlider[activeSliders];
buttonClicker[] buttonClick = new buttonClicker[1];

PFont font;

void setup(){
  
  noLoop();
  noSmooth();
  size(screenW, screenH, OPENGL);
  
  font = loadFont("Verdana-10.vlw");
  initializeSliders();
  initializeButtons();
  
  //////////////////establish twitter connection
  connectTwitter();
  //////////////////establish twitter connection
}

void draw(){
  
  background(0);
  noStroke();
  
  drawModel();
 
  if (buttonClick[0].active==true){
    for (int i=0; i<activeSliders; i++){
      buttonSlide[i].display();
    }
  }
  
  buttonClick[0].display();
  
}
