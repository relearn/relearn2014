float ROTX = 0;                        
float ROTY = 0;
float SCALE = 1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale of the model using the mouse
void mouseDragged() {
  
  if ((mouseX>screenGUI)||(mouseY>(activeSliders+1)*sliderHeight)){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
        SCALE = SCALE+0.1;
     } else {
        SCALE = SCALE-0.1;
     }
     if (SCALE<0.2){ SCALE=0.2;}
     if (SCALE>=10){ SCALE=10;}
     } 
     redraw();
  }
   
  if ((mouseX<screenGUI)&&(mouseY>sliderHeight)&&(buttonClick[0].active==true)&&(mouseX>0)&&(mouseY<(activeSliders+1)*sliderHeight)){
     int button = int(mouseY/sliderHeight)-1;
     buttonSlide[button].update();  
     redraw();  
   }
}

////press spacebar to send a tweet
void keyReleased() {
  if (keyCode == 32 ){
    makeTweetImage=true;
    redraw();
  }
}

////update buttons accroding to mouse clicks
void mouseReleased(){
   if ((mouseX<screenGUI)&&(mouseY<sliderHeight)){
     buttonClick[0].update();  
     println("click");
     redraw();  
   }
}




