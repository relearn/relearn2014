TriangleMesh mesh;
ToxiclibsSupport gfx;

int screenH = 600;
int screenW = 600;
int screenGUI = 0;

ArrayList<Face> facesList;
Table vTable;

void setup() {

 noLoop();
 size(screenW, screenH,OPENGL);
 
 //////////////////establish twitter connection
 connectTwitter();
 //////////////////establish twitter connection
 
 loadStlData();
 getGraphSize();
 sortGraph();

}

void draw() {
  
  background(0);
  
  if(tweetImage==true){
    println("tweeting...");
    tweetNewImage();
    tweetImage=false;
  }
  
  pushMatrix();
    translate(300,300,0);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    drawMesh();
    //drawCSV();
  popMatrix();
  
  noLights();
  image(pgTrans, 0, 0);
  
  
  if(makeTweetImage==true){
    makeTweetImage();
    makeTweetImage=false;
    tweetImage=true;
  }

}
