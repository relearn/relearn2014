Table diffGraph;
PGraphics pg;
PGraphics pgTrans;

int pixColCount;

int maxWidth=0;
int maxWidthTemp=0;

int colCount;

////////////////////////////////////////////////////////////////////////////////////////
///sort the faces data into a vertical pixel graph, accoding to the size of the processing sketch window
///group faces accoring to the average z value of each face, aranged vertically, and stack the data horizontally

////calculate the size of the image, width as well as height that will be needed
////create a .png file of the graph
void getGraphSize(){
  
  for (int j=0; j<height; j++){ 
  //for (int j=height; j>=0; j--){
    
    pixColCount=0;
    maxWidthTemp=0;
    
    for (int i=0; i<vTable.getRowCount(); i++){ 
      ///get average z height of face anbd map to the screen height
      float tempZVal = (vTable.getFloat(i, "z0") + vTable.getFloat(i, "z1") + vTable.getFloat(i, "z2"))/3;
      int pPosZ = int(map(int(tempZVal), minZ, maxZ, 0, height));
      
      if (maxWidthTemp>maxWidth){
        maxWidth=maxWidthTemp;
      }
      if (pPosZ==j){
        maxWidthTemp+=9;
      }  
    }
  }  
  println(maxWidth);
}

////sort the data according to z heights vertically, stacking horizontally
void sortGraph(){
  
  diffGraph = new Table();
  diffGraph.addColumn();
  
  pg = createGraphics(maxWidth, height);
  pg.beginDraw();
  pg.background(255);
 
  pgTrans = createGraphics(maxWidth, height);
  pgTrans.beginDraw();
  pgTrans.background(255,0);
  
  for (int j=0; j<height; j++){ 
  //for (int j=height; j>=0; j--){
    
    pixColCount=0;
    maxWidthTemp=0;
    
    for (int i=0; i<vTable.getRowCount(); i++){ 
      
      ///get average z height of face anbd map to the screen height
      float tempZVal = (vTable.getFloat(i, "z0") + vTable.getFloat(i, "z1") + vTable.getFloat(i, "z2"))/3;
      int pPosZ = int(map(int(tempZVal), minZ, maxZ, 0, height));

      if (pPosZ==j){
        
        ///add columns to the table object to match the pixel data structure 
        if (pixColCount>=diffGraph.getColumnCount()-1){   
          for (int n=0; n<9; n++){
            diffGraph.addColumn();
          }
        }

        for (int k=0; k<3; k++){
          
          String xColumn = "x" + str(k);
          String yColumn = "y" + str(k);
          String zColumn = "z" + str(k);
          
////////////////////////////////////////////////////////////////////////////////X 
          int tempX = int(vTable.getFloat(i, xColumn));
          diffGraph.setFloat(j, pixColCount, tempX);
          String colXTemp = nf(abs(tempX),6, 0);
//          println(tempX);
//          println(colXTemp);
          
          int colXR = int(colXTemp.substring(0,2));
          int colXG = int(colXTemp.substring(2,4));
          int colXB = int(colXTemp.substring(4));
          if (tempX>=0){
            colXR=colXR+100;
            colXG=colXG+100;
            colXB=colXB+100;
          }
          color x = color(colXR, colXG, colXB);
//          println(colXR + "<>" + colXG + "<>" + colXB);  
          pg.set(pixColCount,height-j,x);
          pgTrans.set(pixColCount,height-j,x);   
                   
////////////////////////////////////////////////////////////////////////////////Y       
          int tempY = int(vTable.getFloat(i, yColumn));
          diffGraph.setFloat(j, pixColCount, tempY);
          String colYTemp = nf(abs(tempY),6, 0);
//          println(tempY);
//          println(colYTemp);
                 
          int colYR = int(colYTemp.substring(0,2));   
          int colYG = int(colYTemp.substring(2,4));
          int colYB = int(colYTemp.substring(4));
          if (tempY>=0){
            colYR=colYR+100;
            colYG=colYG+100;
            colYB=colYB+100;
          }
          color y = color(colYR, colYG, colYB);
//          println(colYR + "<>" + colYG + "<>" + colYB);
          pg.set(pixColCount+1,height-j,y);
          pgTrans.set(pixColCount+1,height-j,y);           
         
////////////////////////////////////////////////////////////////////////////////Z             
          int tempZ = int(vTable.getFloat(i, zColumn));
          diffGraph.setFloat(j, pixColCount, tempZ);
          String colZTemp = nf(abs(tempZ),6, 0);
//          println(tempZ);
//          println(colZTemp);
           
          int colZR = int(colZTemp.substring(0,2));  
          int colZG = int(colZTemp.substring(2,4));
          int colZB = int(colZTemp.substring(4));
          if (tempZ>=0){
            colZR=colZR+100;
            colZG=colZG+100;
            colZB=colZB+100;
          }
          color z = color(colZR, colZG, colZB);
//          println(colZR + "<>" + colZG + "<>" + colZB); 
          pg.set(pixColCount+2,height-j,z);
          pgTrans.set(pixColCount+2,height-j,z);  

          pixColCount+=3; 
        }

      }
    }
  }

  pg.endDraw();
  pgTrans.endDraw();
  
  pg.save("data/pixGraph.png");
  //println(maxWidth);

  saveTable(diffGraph, "data/pixData.csv");
  
}





