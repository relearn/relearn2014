boolean tweetImage = false;
boolean makeTweetImage = false;
PGraphics pgTweet;

/////////SEND TWEETS
void tweetNewImage(){ 
  
  String imLoc= savePath("");
  String imName = "data/tweetImage" + ".png";
  //save(imName);
  String tweetText = "...what you lookin' at?? ";
  String tag = "#diffData";
  String message = tweetText + tag;
  
  try {
    
    File imageFile = new File(imLoc + imName);
    println(imageFile);
    StatusUpdate status = new StatusUpdate(message);
    status.setMedia(imageFile);  
    twitterTweet.updateStatus(status);
    println("tweet sent"); 
    
  } catch(TwitterException e) {
    println("Send tweet: " + e + " Status code: " + e.getStatusCode());
  }
}

void makeTweetImage(){
  
//  pgTweet = createGraphics(width, height);
//  pgTweet.beginDraw();
//  pgTweet.background(0);

  String imName = "data/tweetImage" + ".png";
  save(imName);
  
//  pgTweet.save(imName);
//  pgTweet.endDraw();

}

