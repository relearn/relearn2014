1        Constant,‘Unitair Urbanisme,’ unpublished manuscript of lecture at the Stedelijk Museum, Amsterdam,
20 December 1960.

2 Internationale tentoonstelling van experimentele kunst, exhibition at the Stedelijk Museum, Amsterdam, 3-28 November 1949. The original members were Asger ]orn, Christian Dotremont, Ioseph Noiret, Karel Appel, Corneille, and Constant. The name of the group was taken from their three cities: COpenhagen, BRussels and Amsterdam.

3        Constant, Constructies en maquettes, exhibition at the Stedelijk Museum, Amsterdam, 4 May-8 June 1959.

4        Constant, ‘ Le grand jeu a venir,’ Potlatch, no. 30 (15 July 1959). Translated by Gerardo Denis as ‘The Great Game to Come’ in Libero Andreotti and Xavier Costa, eds., Theory of the Derive and other Situationist 
Writings on the City (Barcelona: macba and actar, 1996), pp. 62-65.

5        Constant, ‘New Babylon, een schets voor een cultuur,’ rewrite of a chapter from an unpublished book manuscript, written in German between 1960 and 1965, published in Hans Locher, ed., New Babylon (The Hague: Haags Gemeente­museum, 1974). Translated by John Ham­mond as ‘New Babylon’ in Andreotti and Costa, Theory of the Derive, pp. 154-169, 154.

6        Constant, manuscript of lecture at the ICA, London, 7 November 1963. Partially translated as ‘New Babylon: An Urban­ism of the Future,’ Architectural Design (June 1964), pp. 304-305, 304.

7        Constant, unpublished manuscript of lecture to the Students Association at the Royal Academy of Copenhagen, 12 March 1964.

8        Constant, manuscript of lecture at the ica, London, 7 November 1963.

9        Constant, ‘Het principe van de des­oriëntatie’ (December 1973) in Locher, New Babylon, pp. 65-69. Partially transla­ted by Paul Hammond as ‘The Principle of Disorientation,’ in Libero Andreotti and Xavier Costa, eds., Situationists, Art, Politics, Urbanism (Barcelona: macba and ACTAR, 1996), pp. 86-87.

10        Constant, ‘Description de la zone jaune,’ Internationale Situationniste, no. 4 (June i960), pp. 23-26. Translated by Paul Hammond as ‘Description of the Yellow Zone’ in Andreotti and Costa, Theory of the Dérive, pp. 102-105, 104.

11 ‘Instead of creating coagulated versions of individual desires, like the traditional art—forms used to do, the people in New Babylon will be able to realize immediately any image of their world at any moment.’ Constant, unpublished manuscript of lecture to the Students Association at the Royal Academy of Copenhagen, 12 March 1964.

12        Constant, ‘New Babylon,’ in Andreotti and Costa, Theory of the Dérive, pp. 154-169.

13        Unsigned, ‘Les gratte-ciels par la ra­cine,’ Potlatch, no. g (20 July 1954). Trans­lated by Gerardo Denis as ‘Skyscrapers by the Roots,’ in Andreotti and Costa, Theory of the Dérive, p. 44.

14        Gil Wolman,‘Intervention au Con­gres d’Alba,’ reproduced in facsimile in Gérard Berreby, Documents relatifs à la fondation de l’Internationale Situationniste (Paris: Editions Allia, 1985), pp. 596-598, 597. The ‘positive contributions’ of func­tionalism were later listed: ‘[Unitary urbanism] is not a reaction to functional­ism, but rather a move past it; it is a mat­ter of reaching — beyond the immediate­ly utilitary — an enthralling functional environment. Functionalism — which still has avant-garde pretensions because it continues to encounter outdated resis­tance — has already triumphed to a large extent. Its positive contributions — the adaption to practical functions, technical innovation, comfort, the banishment of superimposed ornament — are today’s banalities.’ Unsigned, ‘L’urbanisme uni­taire à la fin des années 50,’ Internationale Situationniste, no. 3 (December 1959), pp. 1-16. Translated by Thomas Y. Levin as ‘Unitary Urbanism at the End of the 1950s,’ in Elizabeth Sussman, ed., On the Passage of a Few People Through a Rather Brief Moment in Time: The Situationist International 1957-1972 (Cambridge: mit Press, 1989), pp. 143-147,143.

15        ‘La Plate-forme d’Alba,’ Potlatch, no. 27 (2 November 1956). Reproduced in
facsimile in Berreby, Documents relatifs, p. 603. Translated by Ken Knabb as ‘The Alba Platform,’ in Ken Knabb, ed., Situationist International Anthology (Ber­keley: Bureau of Public Secrets, 1981), pp. 14-15. The French and Italian version of the Turin leaflet is reproduced in facsimile in Berreby, Documents relatifs, pp. 604-605.

16        Letter from Guy Debord to Constant, dated 19 January 1957. All cited corres­pondence is part of the Constant Archive, Rijksbureau voor Kunsthistorische Do­cumentatie , The Hague.

17        When Debord receives the catalog, he writes to Constant that he likes it but feels that Willem Sandberg, the museum’s director, has modified it too much to the needs of the museum. Letter from De­bord to Constant, dated 30 April 1959.

18        Constant, ‘The Great Game to Come,’ in Andreotti and Costa, Theory of the Dérive.

19        Guy Debord,‘Première maquettes pour l’urbanisme nouveau,’ Potlatch, no. 30 (15 July 1959). Translated by Gerardo Denis as ‘Preliminary Models for a New Urbanism’ in Andreotti and Costa, Theory of the Dérive, p. 62. The text is unsigned but the original manuscript in Constant’s archive is in Debord’s hand­writing.

20        ‘Le sens du dépérissement de l’art,’ Internationale Situationniste, no. 3 (Decem­ber 1959), pp. 3-8. Translated by John Sheply as ‘The Sense of Decay in Art,’ October, no. 79 (Winter 1997), pp. 102-108.

21        Constant, ‘Une autre ville pour une autre vie,’Internationale Situationniste, no. 3 (December 1959), pp. 37-40. Translated by John Hammond as ‘Another City for Another Life’, in Andreotti and Costa, Theory of the Dérive, pp. 92-95. The essay was originally entitled ‘Notre ambition est dans l'ambiance.’ Debord found the notion of ambience restrictive and proposed ‘Notre ambition est dans la con­struction totale de la vie’ when offering a few corrections to the text.

22        The most developed accounts of the relationship between Constant’s New Babylon and the situationists are to be found in: Simon Sadler, The Situationist City (Cambridge: MIT Press, 1998); Thomas Y. Levin, ‘Geopolitics of Hiber­nation: The Drift of Situationist Urban­ism,’ in Andreotti and Costa, Situationists, Art, Politics, Urbanism, pp. 111-164; Jean- Clarence Lambert, New Babylon: Art et Utopie (Paris: Editions cercle d’art, 1997); Hilde Heynen, ‘New Babylon: The An­tinomies of Utopia,’ Assemblage, no. 29 (April 1996), pp. 24-43; Jean-Clarence Lambert, Constant: Le trois espaces (Paris: Editions cercle d’art, 1992); Peter Wol­len, ‘Bitter Victory: The Art and Politics of the Situationist International,’ in Sussman, On the Passage of a Few People, pp. 20-61.

23        Henry de Bearn, Andre Conard, Mohamed Dahou, Guy Debord, Jacques Fillon, Patrick Straram, Gil Wolman, response to the question ‘Does thought enlighten both us and our actions with the same indifference as the sun, or what is our hope, and what is its value?’ dated 5 May 1954 in La Carte d’après nature, spe­cial issue edited by René Magritte (June 1954).Translated by Nick Tallet.

24        ‘...A new idea in Europe,’ Potlatch, no. 7 (August 1954). The construction oj situations will be the continuous realiza­tion of a great game (...) such a synthe­sis will have to bring together a critique of behavior, a compelling town planning, a mastery of ambiences and relation­ships.’

25        Asger Jorn, ‘Une architecture de la vie,’ Potlatch, no. 15 (22 December 1954). Translated by Gerardo Denis as ‘Archi­tecture for Life,’ in Andreotti and Costa, Theory of the Dérive, p. 51.

26        Guy Debord and Gil Wolman, ‘Pour­quoi le lettrisme?,’ Potlatch, no. 22 (9 Sep­tember, 1955). Translated by Luther Blisset as ‘Why lettrism?’ in Not Bored, no. 27 (1997), pp. 52/57.

27        Guy Debord, ‘Introduction à une cri­tique de la géographie urbaine, Les lèvres nues, no. 6 (September 1955). Translated by Ken Knabb as introduction to a Criti­que of Urban Geography’ in Knabb, Situa­tionist International Anthology, pp. 5-8, 7.

28        Guy Debord, ‘L’architecture et le jeu,’ Potlatch, no. 20 (30 May 1955). Translated by Gerardo Denis as ‘Architecture and Play,’ in Andreotti and Costa, Theory of the Dérive, pp. 53-54.

29        The first issue of Internationale Situa­tionniste offers the following definition of détournement: ‘Short for: détournement of preexisting aesthetic elements. The in­tegration of present or past artistic pro­duction into a superior construction of a milieu. In this sense there can be no situationist painting or music, but only a situationist use of these means. In a more primitive sense, détournement within the old cultural spheres is a method of pro­paganda, a method which testifies to the wearing out and loss of importance of those spheres.’ ‘Définitions,’ Inter natio­nale Situationniste, no. 1 (June 1958), p. 13. Translated by John Hammond as ‘Defi­nitions,’ in Andreotti and Costa, Theory of the Dérive, pp. 68-71.

30        Guy Debord and Gil Wolman,‘Mode d’emploi du détournement,’ Les lèvres nues, no. 8 (May 1956).Translated by Ken Knabb as ‘Methods of Détournement,’ in Knabb, Situationist International Anthology, pp. 8-14, 13.

31        Guy Debord, ‘Théorie de la dérive,’ Les lèvres nues, no. 9 (November 1956).
As Thomas Levin points out, the conclud­ing paragraph containing this sentence was omitted when the article was re­published in Internationale Situationniste, no. 2 (December, 1958).Thomas Levin, ‘Geopolitics of Hibernation: The Drift of Situationist Urbanism,’ in Andreotti and Costa, Situationists, Art, Politics, Urban­ism, p. 145.

32        Guy Debord,‘Rapport sur la con­struction des situations et sur les condi­tions de l’organisation et de l’action de la tendance Situationniste Internationale’ (1957). Partially translated by Ken Knabb as ‘Report on the Construction of Situa­tions and on the International Situationist Tendency’s Conditions of Organization and Action,’ in Knabb, Situationist Inter­national Anthology, pp. 17-25.

33        Gilles Ivain (Ivan Chtcheglov), ‘For­mulaire pour un urbanisme nouveau,’ Internationale Situationniste, no. 1 (June 1958), pp. 15-20.Translated by John Ham­mond as ‘Formulary for a New Urban­ism,’ in Andreotti and Costa, Theory of the Dérive, pp. 14-17.

34        Constant, ‘Sur nos moyens et nos per­spectives,’ Internationale Situationniste, no.2 (December 1958), pp. 23-26.Transla­ted by John Hammond as ‘On our Means and our Perspectives,’ in Andreotti and Costa, Theory oj the Derive, pp. 77-79, 79.

35        ‘Definitions,’ in Andreotti and Costa, Theory of the Derive, pp. 68-71.

36        Unsigned, ‘Unitary Urbanism at the end of the 1950s,’ in Sussman, On the Passage of a Few People. ‘Since the situationist experience of the dérive is simultaneously a means o study of, and a game in, the urban milieu, it is already on the track of unitary urbanism. If unitary urbanism refuses to separate theory from practice, this is not only in orter to promore construction (or research on construction by means of models) along with theoret­ical ideas. The point of such a refusal is above all not to separate the direct, col­lectively experienced playful use of the city from the aspect of urbanism that in­volves construction. The real games and emotions in today’s cities are inseparable from the projects of U U just as later the realized projects of uu should not be isolated from games and emotions that will arise within these accomplishments.’ Ibid., p. 147.

37        Gilles Ivain (Ivan Chtcheglov), ‘For­mulary for a New Urbanism,’ in Andre­otti and Costa, Theory of the Derive, p. 15

38        Guy Debord and Asger Jorn, Memoires (Copenhagen: Permild and Rosengreen, 1959).The book was already prepared in late 1957.

39 Letter from Debord to Constant, dated 4 November 1959. ‘The atmosphere of a few places gave us intimations of the future powers of an architecture it would be necessary to create to be the support and framework for less mediocre games.’ Script of Guy Debord’s 1959 ﬁlm Sur lepassage cle quelques personnes xi traver: une
assez courte unité de temps (On the passage of a few persons through a rather brief period of time). Translated by Ken Knabb in Knabb, Situationist International Anthology, p. 31.

40        Attila Kotanyi and Raoul Vaneigem, ‘Programme élémentaire du bureau d’urbanisme unitaire,’ Internationale Situationniste, no. 6 (August 1961). Translated by Ken Knabb as ‘Elementary Program of the Bureau of Unitary Urban­ism,’ in Knabb, Situationist International Anthology, pp. 65-67, 65.

41        ‘The ex-situationist Constant, whose Dutch collaborators had been excluded from the SI for having agreed to con­struct a church, himself now presents model factories in his catalogue published in March by the Municipal Museum of Bochum. This cunning soul, among two or three plagiarisms of badly understood situationist ideas, shamelessly offers him­self as a public relations man for integrat­ing the masses into capitalist technical civilization; and reproaches the si for having abandoned his whole program for shaking up the urban environment, which he would remain the sole person to be occupied with. And under these conditions! ’ Unsigned, ‘Critique de l’ur­banisme,’ Internationale Situationniste, no. 6 (August 1961), pp. 5-11, 6. Trans­lated by John Hammond as ‘Critique of Urbanism,’ in Andreotti and Costa, Theory of the Derive, pp. 109-115.

42        Unsigned, ‘L’Avant-garde de la pre­sence,’ International Situationniste, no. 8 (January 1963), pp. 14-22.Translated by John Sheply as ‘Editorial Notes: The Avant-Garde of Presence,’ October, no. 79 (Winter 1997), pp. 129-138, 133.

43        Unsigned, ‘L’Opération contre-situ- ationiste dans divers pays,’ Internationale Situationniste, no. 8 (January 1963), pp. 23-29, 29.

44        Aldo van Eyck, ‘Statement Against Rationalism,’ presentation at the ciam congress at Bridgewater, 1947. Reprinted in Sigfried Giedion, A Decade of Modern Architecture (Zürich: Girsberger, 1954), pp. 43-44.

45        Interview with author, Amsterdam, 5 April 1998.

46        Interview with author, Amsterdam, 5 June 1998.

47        B.E., ‘Woon suggesties van architect G. Rietveld in de Bijenkorf te Amster­dam,’ Goed Wonen, no. 7 (1954), pp. 4, 63. R. L. van Oven, ‘Kleurenplan 1954. Rietveld en Constant,’ Interieur, no. 506 (1954), pp. 145-147.The project was also illustrated (without mention of Con­stant) in a special issue of Forum on fur­niture: Forum, no. 4 (1954), p. 183.

48 Constant, ‘Spatiaal colorisme,’ special edition with silkscreens. Edition of 50 (Amsterdam: Stedelijk Museum, 1953). Republished in Forum, no. 10 (1953), pp. 360-361.


49        On 19 July 1955, for example, the Liga distributed the position statements of Constant, Jacob Bakema, Wim Crou- wel, Aldo van Eyck, Auke Kompter, and Gerrit Rietveld for a Liga forum they participated in the week before.

50        Liga exhibitions were held at the Stedelijk Museum, Amsterdam in June 1955, November 1956, November 1958, October 1959, and so on.

51        Nicolas Schoffer and Guy Habasque, Nicolas Schoffer (Neuchâtel: Editions du Griffon, 1963).

52        A large tower by Nicholas Schoffer was part of the first major exhibition of the Groupe Espace in Biot. An illustration of the tower appears in the coverage on the exhibition in l'Architecture d'aujourd'hui, no. 55 (August 1954). 

53        Michel Ragon, Claude Parent: Monogra­phic critique d’un architect (Paris: Bordas, 1982).

54        ‘Architecture is the only art form which is indispensable to man, and which is at the same time functional. As individ­ualist creation can no longer integrate the society we are going towards, it would seem that inevitably the creative arts should retreat into architecture.’ Manuscript by Steven Gilbert entitled ‘ Color Function and Individualist Ex­pression in Art,’ enclosed in a letter from Gilbert to Constant, dated 26 February 1954.

55        Team 10 founded itself in protest to the inadequate treatment of ‘La charte de l’habitat,’ the designated theme of CIAM 9 at Aix-en-Provence in 1953.
For the relevant documents, see Alison Smithson, ed., The Emergence of Team 10 Out of ciam — Documents (London: Archi­tectural Association, 1982).

56        Anthony Hill wrote to Constant sug­gesting that the ‘brilliant young archi­tect’ Peter Smithson, one of the leaders of Team 10, might be sympathetic. Letter from Hill to Constant, dated 1 March 1954.

57        Constant, ‘Art et Habitat,’ unpublish­ed manuscript, 1955.

58        Constant, ‘Van samenwerking naar absolute eenheid van de plastische kunsten,’ Forum, no. 6 (1955), p. 2o7.The article appears immediately after a review of the ‘Nieuw Beelden’ exhibition that is featured on the cover. Ibid. , pp. 203-206.

59        See van Geest, ‘De vrije kunst van het construeren: een tafelontwerp van Constant,‘]ong Holland, vol. 12, no. 2 (1996), pp. 4-11.

60        Constant, ‘Demain la poesie logera la vie’ (manuscript, 1956). Reprinted in Berreby, Documents relatifs, pp. 595-596.

61        Guy Debord, ‘Report on the Con­struction of Situations,’ cited by Levin, ‘Geopolitics of Hibernation,’ in Andreotti and Costa, Situationists, Art, Politics, Urbanism, p. 114.

62        Guy Debord, ‘Report on the Con­struction of Situations,’ in Knabb, Situa­tionist International Anthology, p. 23.

63        Constant, ‘Extraits de lettres, adres­sées par Constant à l’Internationale Si­tuationniste au mois de septembre 1958,’ Constant (Paris: Bibliothèque d’Alexan­drie, 1959). Translated by John Sheply as ‘Extracts from Letters to the Situationist International,’ October, no. 79 (Winter 1997), pp. 96-97.

64        ‘I am coming here this time to pres­ent to you a project so different from my former painting, and to express ideas that in many respects may appear oppo­site to the ideas of Cobra. (...) From the beginning of mechanization, the reaction of the artist has been a negative one. The hatred against the machine that haunted William Morris has stayed alive through modern movements like surrealism and expressionism and goes on in a more recent tendency like action-painting. If, alternatively, the artist decided to con­ciliate with the machine, as was the case with functionalists, he had to give up the freedom of the creative imagination, and thus stopped being an artist at all.’ Con­stant, unpublished manuscript of lecture to the Students Association at the Royal Academy of Copenhagen, 12 December 1964

65        Alison and Peter Smithson, ‘Urban Reidentiﬁcation,’ presented at CIAM 9 in Aix~en—Provence, 24. july 1953. In Smithson, The Emergence af Team io, p. 7.

66        Alison and Peter Smithson,‘Human Associations,’ in Alison and Peter Smith­son, Ordinariness and Light: Urban Theories 1952-1960 and their Application in a Build­ing Project 1963-1970 (Cambridge: mit Press, 1970), pp. 39-61,58.

67        Peter and Alison Smithson, ‘Mobility,’ Architectural Design (October 1958).

68        A. Alberts, “‘Hauptstadt Berlin,” typisch verschijnsel van onze tijd,’ Liga Bulletin (September 1958).

69        Alison and Peter Smithson, ‘Human Associations,’ p. 59.

70        ‘The house, the shell which ﬁts man’s back, looks inward to family and outward to society and its organization should reflect this duality of orientation.The looseness of organization and ease of communication essential to the largest community should be present in this, the smallest. The house is the first deﬁnable city element.’ Alison and Peter Smithson, ‘Human Associations,’ p. 44.

71        Guy Debord, ‘Report on the Construction of Situations,’ in Knabb, Situationist International AnthoIogy, p. 23.

72        ‘When, after a general analysis of the mystification of the evolution of social relations, we realize that the family as we know it is fortunately going to dis­appear, we realize that it is fatal for an architecture that wants to turn itself to the future to have attached its destiny on its conservation.’ Gil Wolman, 'Intervention au congres d’Alba,’ in Berreby, Documents relatifs, pp. 596-598.

73        The first publication of Konrad Wachsmann’s hangers was ‘Etude d’une structure à trois dimensions: un hanger d’aviation,’ L’Architecture d’aujourd’hui (July-August 1954), pp. 4-9.
The article was followed by an analysis of the hangers by Robert de Ricolais and then one of his own projects. Shortly afterwards, Wachsmann’s project ap­peared in Architectural Forum (September 1954).

74        See Thomas McDonough, ‘Situatio­nist Space,’ October, no. 67 (Winter 1994), pp. 59-77.

75        P.H. Chombart de Lauwe, Paris et l’agglomération parisienne (Paris: PUF, 1952). Cited in Constant (Paris: Bibliothè­que d’Alexandrie, 1959).

76        Johan Huizinga, Homo Ludens (Haar­lem: Tjeenk Willink, 1938). Translated as Johan Huizinga, Homo Ludens:A Study of the Play Element in Culture (New York: Harper and Row, 1970). Constant’s con­tact with Huizinga’s theory came during the Cobra years while Debord’s essay on Architecture and Play’ for Potlatch, no. 20 (30 May 1955) begins by citing Huizinga.

77        Constant and Guy Debord, ‘La De­claration d’Amsterdam,’ Internationale Situationniste, no. 2 (December 1958), pp. 31-32. Translated by John Hammond as ‘The Amsterdam Declaration,’ in Andreotti and Costa, Theory of the Derive, p. 80.

78        Constant, ‘De eerste tekenen,’ Liga Bulletin (June 1957).

79        ‘The masses, trapped by their respectfor aesthetic concepts imposed from outside, do not know that they too are capable of creation. This will be awakened by an art which makes suggestions but does not spell anything out, and art which awakens and foresees the associations of images.’ Constant, ‘Manifest van de ex­perimentele groep,’ Reflex, no. 1 (1948), pp. 2-6. Translated as ‘Manifesto,’ in K. Stiles and P. Selz eds., Theories and Do­cuments of Contemporary Art. A Sourcebook of Artists Writings (Berkeley: University of California Press, 1996), pp. 204-208.

80        Constant, ‘Integratie?... van wat?,’ Forum (August 1959), p. 184. Part of the text was used on a Liga invitation to their 1959 exhibition at the Stedelijk Museum. Constant wrote to the editors of Forum on the special issue: ‘The idea of the integration of art and social is something that most avant-garde of the last half century have tried. (...) We need essential change in the structure of society and the phenomenon of artistic creativity to get to integration. The situa - tionists are against individual produc­tions. (...) The production of works of art in the face of today can only be val­uable as a preparation for unitary urban­ism. Every attempt for integration or synthesis without this perspective is doomed to fail in advance.’ Letter dated June 1959.

81        Enrico Castiglioni’s project had been featured, along with those of René Sarger and Frei Otto, in the March 1956 special issue of L’Architecture d’aujourd’hui on ‘Structure.’

82        Letter from Debord to Constant, dated 8 October 1959.

83        Undated letter from Debord to Constant, marked ‘Apres Forum.’

84        ‘In these days where everything, every aspect of life, is becoming more and more repressive, there is one who is par­ticularly repulsive, one who is clearly more on the side of law and order than most. He builds individual living cells, he builds capital cities for the Nepalese, he builds vertical ghettos, he builds morgues for an era that well knows what to do with them, he builds churches .This mo­dulor Protestant, Le Corbusier-Sing- Sing, this dauber of neo-cubist shells, sets in motion ‘machines for living in to the greater glory of God, who created carrion and corbies in his own image and likeness. (...) With Le Corbusier, die interplay and insight that we have a right to expect from truly impressive archi­tecture — disorientation on a daily basis - have been sacrificed in favor of the rubbish chute that will never be used to dispose of the required Bible, already ubiquitous in American hotels. Only a fool would see this as modern architec­ture. It is nothing more than a regres­sion en masse to the old, not properly interred world of Christianity.’ Unsigned, ‘Skyscrapers by the Roots,’ in Andreotti and Costa, Theory of the Dérive, p. 44. ‘It is not worth it to continue to condemn the architecture of Le Corbusier who intended to establish the definite har­mony of a style of capitalist and Christian style of life. (...)’ Gil Wolman, ‘Inter­vention au Congres d’Alba,’ in Berreby, Documents relatifs, p. 597.

85        Unsigned, ‘Unitary Urbanism at the end of the 1950s,’ in Sussman, On the Pas­sage of a Few People, p. 144.

86        See Thomas F. McDonough, ‘Reread­ing Debord, Rereading the Situationists,’ October, no. 79 (Winter 1997), pp. 3-14.

87        Letter from Debord to Constant, dated 28 February 1959.

88        Letter from Debord to Constant, dated 11 March 1959.

89        Constant, ‘Rapport inaugural de la conférence de Munich,’ Internationale Situationniste, no. 3 (December 1959), pp. 29-31.

90        Letter from Debord to Constant, dated 7 September 1959.


