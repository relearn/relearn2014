from collaboration to absolute unity among the plastic arts

First published in 'Van samenwerking tot absolute eenheid van de plastische kunsten', in Forum. Maandblad voor architectuur en gebonden kunst 10, N° 6 (July-August 1955) 207. 

[Mechanization as a way of collaboration between art and architecture, drawn from the visual arts. Reversing of roles (the architect becomes an artist and the artist a constructor), working on THE task (of forming a habitat = fundamental form), no more distinctions (personality, subjectivity and profession). Reaching to the point where the individual doesn’t feel lost while working as a team, a group = first and most important thing about collaboration]

It is useless to talk about collaboration so long as we do not know what demands both architecture and the visual arts must satisfy in order for this collaboration to be of any benefit. In the visual arts domain alone, there already exist such unbridgeable differences that it is impossible to speak of visual art in terms of a single clear concept.

The idea of an amalgamation with architecture arose at a time when visual art had reached a point in its development when the concept of space acquired a more direct significance than it had enjoyed so far. Even so, there would probably have been no question of collaboration had it not been for de Stijl.[1] De Stijl resisted individualism in the visual arts and architecture. An individualism was held responsible for the decline of plastic form in favor of nebulous 'expression.' This Geltungsdrang[2] of the individual, this expressionism, had and still has to be stopped. In architecture, the opponents of expressionism found direct support in the economic advantages of mechanization and, partly because of this, costly expressionism was short-lived there. But in the visual arts, which provided the initial impetus, the situation is rather different.

At the present moment, expressionism -- abstract and figurative -- is on the offensive and, paradoxically enough, is finding some support among architects: waverers who are not philosopher enough to refrain from this unnatural marriage with a visual arts phenomenon that stifles architecture under the pretext of collaboration. Today's architecture is apparently still too impoverished in terms of plasticity not to be afflicted by an inferiority complex when faced with this turbulent stream of artistic hocus-pocus. Salvation must come from the visual arts but not, of course, in this form. 

There is only one possible route to collaboration between architecture and other plastic arts and it is signaled by mechanization, the same mechanization that previously protected architecture from dilution.

Mechanization is in command and the logical consequence of this is a new universal and objective aethetic. The demands of this new aesthetic amount to absolute unity of construction, function, form, and color. The unity of all space-making factors erases the boundaries between the various plastic arts, so rendering further discussion of 'collaboration' superfluous. Architecture has no need of plastic enrichment in the form of decoration or emblems, no need to deck itself in borrowed feathers. No, what architecture draws from the visual arts is a new lifeblood that rejuvenates and strengthens it and allows it to derive artistic benefit from mechanization. Architecture must become a new plastic art whose universal nature enables it to take the place of painting and sculpture which are drowning in subjectivism. A new visual art sufficient in itself and incorporating everything that can objectively be realized in form, color and three-dimensional effects. An art that with a single leap is able to bridge the gap with society because it has, by its very nature, an immediate function that allows it to be assimilated into daily life. An art that appeals to the imagination of the masses because it is able to exploit fully the inexhaustible potential of technology and is thus able to deliver what the modern human being expects of art: harmony, imagination, and a sense of space.

Where does the architect, the visual artist, stand in all this? The time when the community was a sounding board for the individual is over and done with, and the roles are now reversed.

The community sets the individual a task: to form the habitat, a fundamental form that encompasses all facets of life. As soon as one rejects the merging of individual artworks in a more or less impaired whole, and starts to reflect on this new and gigantic task, the distinctive features of personality and profession lose their relevance.

The architect must become an artist and the artist a constructor in order to tackle the creative work as part of a team, together with specialized technicians and engineers. But a lot will have to happen before individual artists are able to work as part of a group without imagining themselves lost. To reach this point is the first and essential step, and perhaps this is also the point of all this talk about collaboration.

[1] Dutch avant-garde group and journal active in the 1920s. De Stijl means The Style. 

[2] German: "validity urge," an urge for validity. 

(Written by Constant and published in Forum vol. 10, no. 6, July-August 1955. Translated from the Dutch by Robyn de Jong-Dalziel. Footnotes by NOT BORED!)


*less distinction between personality and occupation





